# Two latencies used in experiment1 : 10ms (star_10.xml) and 100ms (star_100.xml)

#10ms latency 
results/2vms_10ms inputs/star_10.xml inputs/deployment2.xml 
results/4vms_10ms inputs/star_10.xml inputs/deployment4.xml 
results/8vms_10ms inputs/star_10.xml inputs/deployment8.xml 
results/16vms_10ms inputs/star_10.xml inputs/deployment16.xml 
results/32vms_10ms inputs/star_10.xml inputs/deployment32.xml 
results/48vms_10ms inputs/star_10.xml inputs/deployment48.xml 
results/64vms_10ms inputs/star_10.xml inputs/deployment64.xml 

#100ms latency
results/2vms_100ms inputs/star_100.xml inputs/deployment2.xml 
results/4vms_100ms inputs/star_100.xml inputs/deployment4.xml 
results/8vms_100ms inputs/star_100.xml inputs/deployment8.xml 
results/16vms_100ms inputs/star_100.xml inputs/deployment16.xml 
results/32vms_100ms inputs/star_100.xml inputs/deployment32.xml 
results/48vms_100ms inputs/star_100.xml inputs/deployment48.xml 
results/64vms_100ms inputs/star_100.xml inputs/deployment64.xml 
