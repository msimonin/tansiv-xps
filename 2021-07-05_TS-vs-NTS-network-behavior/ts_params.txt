# CM02
# 1ms latency : 4ms inter node
results/ts_lat_1_gamma_50 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:50331648"
results/ts_lat_1_gamma_12 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
results/ts_lat_1_gamma_10 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:10485760"
results/ts_lat_1_gamma_8 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:8388608"
results/ts_lat_1_gamma_6 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456"
results/ts_lat_1_gamma_4 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:4194304"
results/ts_lat_1_gamma_2 inputs/star_1.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:2097152"

# 2ms latency : 8ms inter node
results/ts_lat_2_gamma_50 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:50331648"
results/ts_lat_2_gamma_12 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
results/ts_lat_2_gamma_10 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:10485760"
results/ts_lat_2_gamma_8 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:8388608"
results/ts_lat_2_gamma_6 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456"
results/ts_lat_2_gamma_4 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:4194304"
results/ts_lat_2_gamma_2 inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:2097152"

# 10ms latency : 40ms inter node
results/ts_lat_10_gamma_50 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:50331648"
results/ts_lat_10_gamma_12 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
results/ts_lat_10_gamma_10 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:10485760"
results/ts_lat_10_gamma_8 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:8388608"
results/ts_lat_10_gamma_6 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456"
results/ts_lat_10_gamma_4 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:4194304"
results/ts_lat_10_gamma_2 inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:2097152"

# 100ms latency : 400ms inter node
results/ts_lat_100_gamma_50 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:50331648"
results/ts_lat_100_gamma_12 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912"
results/ts_lat_100_gamma_10 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:10485760"
results/ts_lat_100_gamma_8 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:8388608"
results/ts_lat_100_gamma_6 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456"
results/ts_lat_100_gamma_4 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:4194304"
results/ts_lat_100_gamma_2 inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:2097152"


## precision
# 2ms latency / precision 1us
results/ts_lat_2_gamma_6_surf_1us inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000001"
results/ts_lat_2_gamma_12_surf_1us inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000001"

# 2ms latency / precision 100ns
results/ts_lat_2_gamma_6_surf_100ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.0000001"
results/ts_lat_2_gamma_12_surf_100ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.0000001"

# 2ms latency / precision 10ns
results/ts_lat_2_gamma_6_surf_10ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.00000001"
results/ts_lat_2_gamma_12_surf_10ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.00000001"

# 2ms latency / precision 1ns
results/ts_lat_2_gamma_6_surf_1ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000000001"
results/ts_lat_2_gamma_12_surf_1ns inputs/star_2.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000000001"

##
# 10ms latency / precision 1us
results/ts_lat_10_gamma_6_surf_1us inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000001"
results/ts_lat_10_gamma_12_surf_1us inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000001"

# 10ms latency / precision 100ns
results/ts_lat_10_gamma_6_surf_100ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.0000001"
results/ts_lat_10_gamma_12_surf_100ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.0000001"

# 10ms latency / precision 10ns
results/ts_lat_10_gamma_6_surf_10ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.00000001"
results/ts_lat_10_gamma_12_surf_10ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.00000001"

# 10ms latency / precision 1ns
results/ts_lat_10_gamma_6_surf_1ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000000001"
results/ts_lat_10_gamma_12_surf_1ns inputs/star_10.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000000001"

##
# 100ms latency / precision 1us
results/ts_lat_100_gamma_6_surf_1us inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000001"
results/ts_lat_100_gamma_12_surf_1us inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000001"

# 100ms latency / precision 100ns
results/ts_lat_100_gamma_6_surf_100ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.0000001"
results/ts_lat_100_gamma_12_surf_100ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.0000001"

# 100ms latency / precision 10ns
results/ts_lat_100_gamma_6_surf_10ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.00000001"
results/ts_lat_100_gamma_12_surf_10ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.00000001"

# 100ms latency / precision 1ns
results/ts_lat_100_gamma_6_surf_1ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:6291456 --cfg=surf/precision:0.000000001"
results/ts_lat_100_gamma_12_surf_1ns inputs/star_100.xml inputs/deployment_2.xml "--cfg=network/model:CM02 --cfg=network/TCP-gamma:12582912 --cfg=surf/precision:0.000000001"
