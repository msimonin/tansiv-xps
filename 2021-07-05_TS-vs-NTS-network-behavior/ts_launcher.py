#!/home/lorilling/tansiv-xps/2021-07-05_TS-vs-NTS-network-behavior/venv/bin/python3

#OAR -l nodes=1,walltime=05:00:00
#OAR -p cluster='paravance'



# Invocation
# directory platform deployment simgrid_args

from pathlib import Path
from subprocess import check_call
import sys
import os

from tansiv.undercloud import localhost
from tansiv.api import start_ts, flent, dump
from tansiv.constants import QEMU_IMAGE, DEFAULT_DOCKER_IMAGE

# give me the power
# this is required by the start_* function
check_call("sudo-g5k")

print(sys.argv)
_, directory, platform, deployment, simgrid_args = sys.argv

# fail fast if the directory exists
working_dir = Path(directory).resolve()
working_dir.mkdir(parents=True, exist_ok=True)

platform_path = Path(platform).resolve()
deployment_path = Path(deployment).resolve()

os.chdir(working_dir)


(working_dir / "cmd").write_text(" ".join(sys.argv))
(working_dir / "deployment.xml").write_text(deployment_path.read_text())
(working_dir / "platform.xml").write_text(platform_path.read_text())


roles, provider = localhost()
registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
tansiv_roles, _ = start_ts(roles,
        str(platform_path),
        str(deployment_path),
        QEMU_IMAGE,
        DEFAULT_DOCKER_IMAGE,
        simgrid_args=simgrid_args,
        docker_registry_opts=registry_opts,
        )

for _ in range(10):
    flent(tansiv_roles, working_dir=working_dir, duration=60)

dump(roles, tansiv_roles, working_dir=working_dir)
