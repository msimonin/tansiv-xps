# 10ms latency
result_nts/2vms_10ms 2 "delay 10ms rate 1gbit"
result_nts/4vms_10ms 4 "delay 10ms rate 1gbit"
result_nts/8vms_10ms 8 "delay 10ms rate 1gbit"
result_nts/16vms_10ms 16 "delay 10ms rate 1gbit"
result_nts/32vms_10ms 32 "delay 10ms rate 1gbit"
result_nts/48vms_10ms 48 "delay 10ms rate 1gbit"
result_nts/64vms_10ms 64 "delay 10ms rate 1gbit"

# 100ms latency
result_nts/2vms_100ms 2 "delay 100ms rate 1gbit"
result_nts/4vms_100ms 4 "delay 100ms rate 1gbit"
result_nts/8vms_100ms 8 "delay 100ms rate 1gbit"
result_nts/16vms_100ms 16 "delay 100ms rate 1gbit"
result_nts/32vms_100ms 32 "delay 100ms rate 1gbit"
result_nts/48vms_100ms 48 "delay 100ms rate 1gbit"
result_nts/64vms_100ms 64 "delay 100ms rate 1gbit"


